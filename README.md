# polyconsole

C++/Polycode sample project with built-in utilities like a console to type in Lua/Polycode commands.

**This repository has [moved](https://github.com/mcclure/bitbucket-backup/tree/archive/repos/polyconsole).**
